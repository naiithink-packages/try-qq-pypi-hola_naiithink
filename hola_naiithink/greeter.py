# -*- coding: utf-8; mode: python; -*-


class Hola:
    def __init__(self, name="World"):
        self.name = name
    
    def say_hello(self):
        print(f"Hello, {self.name.capitalize()}!")

    def say_bye(self):
        print(f"Bye, {self.name.capitalize()}.  See you soon!")


if __name__ == "__main__":
    me = Hola("Foo")
    me.say_hello()
    me.say_bye()
